<?php

namespace spec\Ekoxe\DDDUtil\Domain;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Ekoxe\DDDUtil\Domain\DomainEvent;
use Ekoxe\DDDUtil\Domain\DomainEventSubscriber;

class SimplePublisherSpec extends ObjectBehavior
{
    function let(DomainEventSubscriber $subscribedEventSubscriber, DomainEventSubscriber $notSubscribedEventSubscriber, DomainEvent $event)
    {
        $subscribedEventSubscriber->isSubscribedTo($event)->shouldBeCalled()->willReturn(true);

        $this->subscribe($subscribedEventSubscriber);
    }

    function it_publishes_an_event_to_its_subscribers_given_they_are_subscribed_to_event($subscribedEventSubscriber, $event)
    {
        $subscribedEventSubscriber->handle($event)->shouldBeCalled();

        $this->publish($event);
    }

    function it_does_not_publish_an_event_to_a_subscriber_given_it_is_not_subscribed_to_event($subscribedEventSubscriber, $notSubscribedEventSubscriber, $event)
    {
        $subscribedEventSubscriber->handle($event)->shouldBeCalled();
        $notSubscribedEventSubscriber->handle($event)->shouldNotBeCalled();

        $this->publish($event);
    }
}
