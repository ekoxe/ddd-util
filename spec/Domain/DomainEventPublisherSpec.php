<?php

namespace spec\Ekoxe\DDDUtil\Domain;

use Ekoxe\DDDUtil\Domain\DomainEvent;
use Ekoxe\DDDUtil\Domain\DomainEventPublisher;
use Ekoxe\DDDUtil\Domain\DomainEventSubscriber;
use Ekoxe\DDDUtil\Domain\Publisher;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class DomainEventPublisherSpec extends ObjectBehavior
{
    private $publisher;

    function let(Publisher $publisher)
    {
        $this->beConstructedThrough('publish');

        $this->setPublisher($publisher);
        $this->publisher = $publisher;
    }

    function it_subscribes_a_domain_event_subscriber(DomainEventSubscriber $subscriber)
    {
        $this->subscribe($subscriber);

        $this->publisher->subscribe($subscriber)->shouldHaveBeenCalled();
    }

    function it_publishes_a_domain_event(DomainEvent $event)
    {
        $this->that($event);

        $this->publisher->publish($event)->shouldHaveBeenCalled();
    }

    function it_unsubscribes_a_domain_event_subscriber(DomainEventSubscriber $subscriber)
    {
        $this->unsubscribe($subscriber);

        $this->publisher->unsubscribe($subscriber)->shouldHaveBeenCalled();
    }
}
