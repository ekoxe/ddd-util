<?php

namespace spec\Ekoxe\DDDUtil\Helper;

use Ekoxe\DDDUtil\Domain\DomainEvent;
use PhpSpec\ObjectBehavior;

class SpySubscriberSpec extends ObjectBehavior
{
    function it_is_subscribed_to_any_domain_event(DomainEvent $event)
    {
        $this->shouldBeSubscribedTo($event);
    }

    function it_tracks_received_event_when_handling_event(DomainEvent $event, DomainEvent $anotherEvent)
    {
        $this->handle($event);
        $this->receivedEvent->shouldBe($event);

        $this->handle($anotherEvent);
        $this->receivedEvent->shouldBe($anotherEvent);
    }
}
