<?php


namespace Ekoxe\DDDUtil\Helper;


use Ekoxe\DDDUtil\Domain\DomainEvent;
use Ekoxe\DDDUtil\Domain\DomainEventSubscriber;

class SpySubscriber implements DomainEventSubscriber
{
    public $receivedEvent;

    public function handle(DomainEvent $event)
    {
        $this->receivedEvent = $event;
    }

    public function isSubscribedTo(DomainEvent $event): bool
    {
        return true;
    }
}