<?php


namespace Ekoxe\DDDUtil\Domain;


class SimplePublisher implements Publisher
{
    private $subscribers = [];

    public function subscribe(DomainEventSubscriber $domainEventSubscriber)
    {
        $this->subscribers[] = $domainEventSubscriber;
    }

    public function unsubscribe(DomainEventSubscriber $domainEventSubscriber)
    {
        foreach ($this->subscribers as $key => $subscriber) {
            if ($subscriber === $domainEventSubscriber) {
                unset($this->subscribers[$key]);
            }
        }
    }

    public function publish(DomainEvent $event)
    {
        foreach ($this->subscribers as $aSubscriber) {
            if ($aSubscriber->isSubscribedTo($event)) {
                $aSubscriber->handle($event);
            }
        }
    }
}