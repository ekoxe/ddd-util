<?php


namespace Ekoxe\DDDUtil\Domain;


interface DomainEventSubscriber
{
    public function handle(DomainEvent $event);

    public function isSubscribedTo(DomainEvent $event): bool;
}