<?php


namespace Ekoxe\DDDUtil\Domain;


final class DomainEventPublisher
{
    private static $instance;
    private $publisher;

    private function __construct()
    {
        $this->publisher = new SimplePublisher();
    }

    public static function setPublisher($publisher)
    {
        self::publish()->publisher = $publisher;
    }

    public static function publish(): DomainEventPublisher
    {
        if (self::$instance == null) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function subscribe(DomainEventSubscriber $subscriber)
    {
        $this->publisher->subscribe($subscriber);
    }

    public function that(DomainEvent $domainEvent)
    {
        $this->publisher->publish($domainEvent);
    }

    public function unsubscribe(DomainEventSubscriber $subscriber)
    {
        $this->publisher->unsubscribe($subscriber);
    }

    public function __clone()
    {
        throw new \BadMethodCallException('Cannot clone a Singleton');
    }
}