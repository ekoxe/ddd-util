<?php


namespace Ekoxe\DDDUtil\Domain;


abstract class DomainEvent
{
    private $occurredOn;

    public function __construct(\DateTimeImmutable $occurredOn)
    {
        $this->occurredOn = $occurredOn;
    }

    public function occurredOn(): \DateTimeImmutable {
        return $this->occurredOn;
    }
}