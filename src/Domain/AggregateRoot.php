<?php


namespace Ekoxe\DDDUtil\Domain;


abstract class AggregateRoot
{
    private $recordedEvents = [];

    protected function recordApplyAndPublishThat(DomainEvent $domainEvent)
    {
        $this->recordThat($domainEvent);
        $this->applyThat($domainEvent);
        $this->publishThat($domainEvent);
    }

    private function recordThat(DomainEvent $domainEvent)
    {
        $this->recordedEvents[] = $domainEvent;
    }

    private function applyThat(DomainEvent $domainEvent)
    {
        $className = get_class($domainEvent);
        $className = substr($className, strrpos($className, '\\') + 1);
        $modifier = 'apply' . $className;
        $this->$modifier($domainEvent);
    }

    private function publishThat(DomainEvent $domainEvent)
    {
        DomainEventPublisher::publish()->that($domainEvent);
    }

    public function recordedEvents()
    {
        return $this->recordedEvents;
    }

    public function clearEvents()
    {
        $this->recordedEvents = [];
    }
}