<?php
namespace Ekoxe\DDDUtil\Domain;

interface Publisher
{
    public function subscribe(DomainEventSubscriber $domainEventSubscriber);

    public function unsubscribe(DomainEventSubscriber $domainEventSubscriber);

    public function publish(DomainEvent $event);
}